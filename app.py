from serpapi import GoogleSearch
from PIL import Image
import PIL.Image
import requests
import streamlit as st
hide_streamlit_style = """
<style>
#MainMenu {visibility: hidden;}
footer {visibility: hidden;}
header {visibility: hidden;}
</style>

"""
st.markdown(hide_streamlit_style, unsafe_allow_html=True)


def reverse_image_search(image_url):
    # Call API
    params = {
        "engine": "google_reverse_image",
        "image_url": image_url,
        "api_key": "2fe54ca2a2675de6516180ae0dfea3af8843fcd7e10fdb52832f24375d6f2dad"
    }
    search = GoogleSearch(params)
    results = search.get_dict()
    inline_images = results["inline_images"]

    # Extract and return result
    max_len = len(inline_images)
    i = 0
    list_img = []
    list_source_link = []
    list_title = []

    while i < max_len and len(list_img) < 5:
        img_url = inline_images[i]['original']
        img_source = inline_images[i]['source']
        img_title = inline_images[i]['title']
        try:
            img = Image.open(requests.get(img_url, stream=True).raw)
            newsize = (800, 500)
            img = img.resize(newsize)
            list_img.append(img)
            list_source_link.append(img_source)
            list_title.append(img_title)
            i += 1
        except:
            # print('Fail to load image from url: '+ img_url)
            i += 1
            continue
    # Return
    output_dict = {}
    original_img = PIL.Image.open(requests.get(image_url, stream=True).raw)
    newsize = (800, 500)
    original_img = original_img.resize(newsize)
    output_dict['original'] = original_img

    for i in range(len(list_img)):
        name_image = 'image_' + str(i + 1)
        name_title = 'title_' + str(i + 1)
        name_url = 'url_' + str(i + 1)

        output_dict[name_image] = list_img[i]
        output_dict[name_title] = list_title[i]
        output_dict[name_url] = list_source_link[i]

    return (output_dict)


URL = st.text_input('Enter the image URL and press Enter')


if URL != '':

    try:
        with st.spinner("Analyzing...Please Wait"):
            output = reverse_image_search(URL)
        st.success('Analysis completed!')
        st.image(output['original'], caption='Input image')
        st.header("5 most similar images")

        tab1, tab2, tab3, tab4, tab5 = st.tabs(
            ["Image1", "Image2", "Image3", "Image4", "Image5"])
        with tab1:
            st.image(output['image_1'], caption=output['title_1'])
            url_1 = output['url_1']
            st.markdown("check out this [source](%s)" % url_1)

        with tab2:
            st.image(output['image_2'], caption=output['title_2'])
            url_2 = output['url_2']
            st.markdown("check out this [source](%s)" % url_2)

        with tab3:
            st.image(output['image_3'], caption=output['title_3'])
            url_3 = output['url_3']
            st.markdown("check out this [source](%s)" % url_3)

        with tab4:
            st.image(output['image_4'], caption=output['title_4'])
            url_4 = output['url_4']
            st.markdown("check out this [source](%s)" % url_4)

        with tab5:
            st.image(output['image_5'], caption=output['title_5'])
            url_5 = output['url_5']
            st.markdown("check out this [source](%s)" % url_5)
    except:
        st.error("Can't find matching image, please change another URL")
        st.stop()

else:
    st.header('Example')
    st.markdown('**--- Instruction Guide  ---**')
    st.markdown('**--- Here is the input image ---**')
    ori_img = PIL.Image.open("original.jpeg")
    st.image(ori_img, caption='Input image')

    st.markdown('**--- Here is the result section ---**')
    st.header("5 most similar images")

    tab1, tab2, tab3, tab4, tab5 = st.tabs(
        ["Image1", "Image2", "Image3", "Image4", "Image5"])
    with tab1:
        st.markdown(
            '**--- Here is one of the result image, click the tab above to switch to another result ---**')
        img_1 = PIL.Image.open("1.jpeg")
        st.image(img_1, caption='Python 與OpenCV 裁切圖片教學- G. T. Wang')
        url_1 = 'https://blog.gtwang.org/programming/how-to-crop-an-image-in-opencv-using-python/'
        st.markdown(
            '**--- The caption is the title of the source of the image ---**')
        st.markdown("check out this [source](%s)" % url_1)
        st.markdown(
            '**--- Click the hyperlink will direct to the source of the image ---**')

    with tab2:
        st.markdown(
            '**--- Here is one of the result image, click the tab above to switch to another result ---**')
        img_2 = PIL.Image.open("2.jpeg")
        st.image(img_2, caption='py-opencv 画像の一部を切り抜いて保存する - Symfoware')
        url_2 = 'https://symfoware.blog.fc2.com/blog-entry-1524.html'
        st.markdown(
            '**--- The caption is the title of the source of the image ---**')
        st.markdown("check out this [source](%s)" % url_2)
        st.markdown(
            '**--- Click the hyperlink will direct to the source of the image ---**')

    with tab3:
        st.markdown(
            '**--- Here is one of the result image, click the tab above to switch to another result ---**')
        img_3 = PIL.Image.open("3.jpeg")
        st.image(img_3, caption='OpenCV – Görüntü Okuma ve Yazma – Hakan CERAN')
        url_3 = 'https://hakanceran.com.tr/opencv-goruntu-okuma-ve-yazma/'
        st.markdown(
            '**--- The caption is the title of the source of the image ---**')
        st.markdown("check out this [source](%s)" % url_3)
        st.markdown(
            '**--- Click the hyperlink will direct to the source of the image ---**')

    with tab4:
        st.markdown(
            '**--- Here is one of the result image, click the tab above to switch to another result ---**')
        img_4 = PIL.Image.open("4.jpeg")
        st.image(img_4, caption='数字图像处理（2）——环境搭建（MATLAB、OpenCV、Python）_天狭鬼 ...')
        st.markdown(
            '**--- The caption is the title of the source of the image ---**')
        url_4 = 'https://blog.csdn.net/m0_37763336/article/details/101672798'
        st.markdown("check out this [source](%s)" % url_4)
        st.markdown(
            '**--- Click the hyperlink will direct to the source of the image ---**')

    with tab5:
        st.markdown(
            '**--- Here is one of the result image, click the tab above to switch to another result ---**')
        img_5 = PIL.Image.open("5.jpeg")
        st.image(img_5, caption='人脸识别检测戴口罩实战】初识OpenCV简单操作，图片的读取和显示 ...')
        st.markdown(
            '**--- The caption is the title of the source of the image ---**')
        url_5 = 'https://juejin.cn/post/7115984461736443911'
        st.markdown("check out this [source](%s)" % url_5)
        st.markdown(
            '**--- Click the hyperlink will direct to the source of the image ---**')
